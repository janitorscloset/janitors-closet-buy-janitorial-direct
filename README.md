Buy Janitorial Direct sells equipment and supplies for Janitorial Professionals, Residential and Commercial Carpet Cleaning Professionals, In-House Service Providers, Disaster Restoration Contractors, Abatement Contractors, and Building Service Contractors.

Address: 4183 E Hillsborough Ave, Tampa, FL 33610, USA

Phone: 813-626-3030

Website: https://www.buyjanitorialdirect.com